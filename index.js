const db = firebase.firestore();
const dbRecetas = db.collection('recetas');

const auth = firebase.auth();

// console.log('db ', db);
// let id = '';

// let ingredientes =[];

// const inputIngrediente = document.getElementById('input-ingrediente');
// const containerIngredientes = document.getElementById('container-ingredientes');




// TABS
// const tabsLinks = document.querySelectorAll('.nav-link');
// const tabsPane = document.querySelectorAll('.tab-pane');
// tabsLinks.forEach( link => {
//     link.addEventListener('click', (e) => {
//         // levanto la pagina del dataset y recorro los tabs para mostrar/ocultar
//         const page = e.target.dataset.page;
//         tabsPane.forEach( pane => {
//             if(page === pane.id){
//                 pane.classList.add('show','active');
//             } else {
//                 pane.classList.remove('show','active');
//             }
//         });
//         tabsLinks.forEach( lnk => {
//             if(lnk === link){
//                 lnk.classList.add('active');
//             } else {
//                 lnk.classList.remove('active');
//             }
//         });
//     });
// });

// SERVICIO RECETAS
const taskForm = document.getElementById('receta-form');
const tasksContainer = document.getElementById('receta-filas');
const saveButton = document.getElementById('btn-receta-form');

const saveReceta = (title, tipo, ingredientes) => {
    id='';
    dbRecetas.doc().set({
        title,
        tipo,
        ingredientes
    });
}
const updateReceta = (id, updatedReceta) => dbRecetas.doc(id).set({
    title: updatedReceta.title,
    tipo: updatedReceta.tipo,
    ingredientes: updatedReceta.ingredientes
});

const getRecetas = () => dbRecetas.get();
const getReceta = (id) => dbRecetas.doc(id).get();

const onGetReceta = (callback) => dbRecetas.onSnapshot(callback);

const deletaReceta = id => dbRecetas.doc(id).delete();
// fin SERVICIO RECETAS

// Listado de recetas
window.addEventListener('DOMContentLoaded', async (e) => {
    onGetReceta( (querySnapshot) => {
        tasksContainer.innerHTML = '';
        querySnapshot.forEach( doc => {
            const receta = doc.data();
            receta.id = doc.id;
            tasksContainer.innerHTML += `
            <tr class="${receta.tipo=='Comida'? 'table-active':''}">
                <th scope="row">1</th>
                <td>${receta.tipo}</td>
                <td>${receta.title}</td>
                <td class="col-2">
                    <button class="btn btn-secondary btn-sm btn-delete" data-id="${receta.id}">Borrar</button>
                    <button class="btn btn-primary btn-sm btn-edit" data-id="${receta.id}">Editar</button>
                </td>
            </tr>
            `;

            const btnsDelete = document.querySelectorAll('.btn-delete');
            btnsDelete.forEach( btn => {
                btn.addEventListener('click', (e) => {
                    console.log('borrar ', e.target.dataset.id)
                    deletaReceta(e.target.dataset.id);
                });
            });

            const btnsEdit = document.querySelectorAll('.btn-edit');
            btnsEdit.forEach( btn => {
                btn.addEventListener('click', async (e) => {
                    const doc = await getReceta(e.target.dataset.id);
                    id = doc.id;
                    // console.log('editar ', doc);
                    taskForm['receta-tipo'].value = doc.data().tipo;
                    taskForm['receta-title'].value = doc.data().title;
                    
                    containerIngredientes.innerHTML = '';
                    if(doc.data().ingredientes){
                        doc.data().ingredientes.forEach(ingrediente => {
                            containerIngredientes.innerHTML += `
                            <span class="badge bg-light tag-ingrediente">
                                ${ ingrediente }
                            </span>`;
                        });
                    }
                })
            })
        })
    });
})


// grabar formulario
// saveButton.addEventListener('click', async e => {
//     e.preventDefault();
//     const tipo = taskForm['receta-tipo'].value;
//     const title = taskForm['receta-title'].value;

    // let ingredientesTags = document.querySelectorAll('.tag-ingrediente');
    // ingredientesTags.forEach(tag => {
    //     ingredientes.push(tag.innerText);
    // });
    
    // if(!id){
    //     await saveReceta(title, tipo, ingredientes);
    // } else {
    //     await updateReceta(id, {title, tipo, ingredientes});
    // }
    // id = '';
    // ingredientes = [];

    // taskForm.reset();
    // taskForm['receta-title'].focus;
    // document.getElementById('container-ingredientes').innerHTML ='';
// 
//     showModal();
// });

// inputIngrediente.addEventListener('keyup', async e => {
//     e.preventDefault();
//     const ingrediente = taskForm['input-ingrediente'].value;
//     if (e.key === "Enter") {
//         // console.log('input ', ingrediente);
//         containerIngredientes.innerHTML += `
//             <span class="badge bg-light tag-ingrediente">
//                 ${ ingrediente }
//             </span>`;
//         taskForm['input-ingrediente'].value = '';

//         const ingredientesTags = document.querySelectorAll('.tag-ingrediente');
//         ingredientesTags.forEach( tag => {
//             // BORRAR LOS INGREDIENTES
//             tag.addEventListener('click', (e) => {
//                 console.log('borrar ingred ', e.target);
//                 e.target.innerHTML = '';
//             });
//         });
//     }

    // taskForm.reset();
    // taskForm['receta-title'].focus;
// });


// TABLA SEMANAL
// const semanaForm = document.getElementById('semana-form');
// const semanaTable = document.getElementById('semana-table-body');
// const semanaGenerarBtn = document.getElementById('btn-semana-generar');
// const semanaGuardarBtn = document.getElementById('btn-semana-guardar');

// grabar formulario
// semanaGenerarBtn.addEventListener('click', async e => {
//     e.preventDefault();
//     const cantPos = 7;
//     const matriz = [];
//     const recetasSnapshot = await dbRecetas.where("tipo", "==", "Comida").get();
    
//     for(let i=0; i < cantPos; i++){
//         // Returns a random integer from 0 to receta results:
//         let posicion1 = Math.floor(Math.random() * recetasSnapshot.size);
//         let posicion2 = Math.floor(Math.random() * recetasSnapshot.size);
//         matriz.push([
//             recetasSnapshot.docs[posicion1].data(),
//             recetasSnapshot.docs[posicion2].data()
//         ]);
//     }
    
//     semanaTable.innerHTML = '';
//     matriz.forEach( (array, index) => {
//         semanaTable.innerHTML += `
//             <tr>
//                 <th class="col-2">${ index + 1 }</th>
//                 <td class="col-5">${ array[0].title }</td>
//                 <td class="col-5">${ array[1].title }</td>
//             </tr>`;
//     });
    
// });

// SINGUP
const singupButton = document.getElementById('singup');
singupButton.addEventListener('click', async e => {
    e.preventDefault();
    const email = document.getElementById('singup-email');
    const password = document.getElementById('singup-password');

    if(!email.value || !password.value){
        alert('email o pass inválido', 'danger');
    }
    
    auth.createUserWithEmailAndPassword(email.value, password.value)
        .then( credential => {
            email.value='';
            password.value = '';
            console.log(credential);
            // credential.additionalUserInfo
            // credential.credential
            // credential.operationType
            // credential.user.uid
            // credential.user.refreshToken
            closeSingup();
        })
        .catch(error => {console.log(error.message); alert(error.message, 'danger')});
});

// LOGIN
const loginButton = document.getElementById('login');
loginButton.addEventListener('click', async e => {
    e.preventDefault();
    const email = document.getElementById('login-email');
    const password = document.getElementById('login-password');

    if(!email.value || !password.value){
        alert('email o pass inválido', 'danger');
    }
    
    auth.signInWithEmailAndPassword(email.value, password.value)
        .then( credential => {
            console.log(credential);
            // credential.additionalUserInfo
            // credential.credential
            // credential.operationType
            // credential.user.uid
            // credential.user.refreshToken
            closeLogin();
            // btn-modal-logout
            document.querySelector('#btn-modal-login').classList.add('d-none');
            document.querySelector('#btn-modal-logout').classList.remove('d-none');
        })
        .catch(error => {console.log(error.message); alert(error.message, 'danger')});
});

const logout = () => {
    auth.signOut().then( () => {
        document.querySelector('#btn-modal-logout').classList.add('d-none');
        document.querySelector('#btn-modal-login').classList.remove('d-none');
    });
}

const showCustomModal = (props, callback) => {
    let myModal = new bootstrap.Modal(document.getElementById('modal-custom'), {
        keyboard: false
    });
    let footerBackup = document.getElementById('modal-footer').innerHTML;
    document.getElementById('modal-custom').addEventListener('hidden.bs.modal', () => {
        document.getElementById('modal-footer').innerHTML = footerBackup;
    });

    if(myModal){
        if(props){
            const modalTitle = document.getElementById('modal-title');
            modalTitle.innerText = props.title? props.title : '';
            const modalBody = document.getElementById('modal-body');
            modalBody.innerText = props.message? props.message : '';
            const modalFooter = document.getElementById('modal-footer');
            props.buttons.forEach(button => {
                modalFooter.appendChild(button)
            });
        }
        myModal.show();
    }

}


const showLogin = (config) => {
    closeSingup();
    document.querySelector('#btn-modal-login').click();
}
const closeLogin = (config) => {
    document.querySelector('#btn-close-login').click();
}

const showSingup = (config) => {
    closeLogin();
    document.querySelector('#btn-modal-singup').click();
}
const closeSingup = () => {
    document.querySelector('#btn-close-singup').click();
}

const showLogout = (config) => {
    const btn = document.createElement("button");
    btn.classList.add("btn");
    btn.classList.add("btn-primary");
    btn.classList.remove("navbar-toggler");
    btn.innerText = 'Logout';
    btn.addEventListener("click", function () {
        logout();
        closeLogout();
    });
    showCustomModal({title: 'Logout', message: '¿Está seguro que desea cerrar sesión?', buttons: [btn]});
}
const closeLogout = () => {
    document.querySelector('#btn-close-custom').click();
}

// ALERT
// div
const alertPlaceholder = document.getElementById('alertPlaceholder'); 
// creacion del alert
const alert = (message, type) => {
    const wrapper = document.createElement('div')
    wrapper.innerHTML = [
      `<div class="alert alert-${type} alert-dismissible alert-width alert-fixed" role="alert">`,
      `   <div>${message}</div>`,
      '   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>',
      '</div>'
    ].join('')
  
    alertPlaceholder.append(wrapper)
}

// const showAlert = () => {

//     $('.alert').alert()
// }
// const closeAlert = () => {
//     $('.alert').alert('close')
// }