let mic, img, escala;
let boca;

function setup() {
  createCanvas(710, 700);
  escala = 0.5;
  img = loadImage('./assets/avatar.png'); // Cargar la imagen
  boca = [
    loadImage('./assets/boca03.png'),
    loadImage('./assets/boca04.png'),
    loadImage('./assets/boca02.png'),
    loadImage('./assets/boca01.png'),
    loadImage('./assets/boca05.png'),
    loadImage('./assets/boca06.png'),
    loadImage('./assets/boca07.png'),
    loadImage('./assets/boca10.png'),
    loadImage('./assets/boca11.png'),
    loadImage('./assets/boca12.png'),
    loadImage('./assets/boca13.png'),
    loadImage('./assets/boca14.png'),
    loadImage('./assets/boca15.png'),
    loadImage('./assets/boca16.png'),
    loadImage('./assets/boca17.png'),
    loadImage('./assets/boca18.png'),
    loadImage('./assets/boca19.png'),
    loadImage('./assets/boca20.png')
  ]


  // crea una entrada de audio
  mic = new p5.AudioIn();

  // inicia la entrada de audio
  // por defecto, no la conecta (.connect()) a los parlantes del computador.
  mic.start();
}

function draw() {
  background(200);

  // obtén el volumen general (entre 0.0 y 1.0)
  let vol = mic.getLevel();
  fill(127);
  //   stroke(0);
  // dibuja una elipse con altura según el volumen
  image(img, 200,300, 300, 300);
  // let h = map(vol, 0, 1, 0, height);
    // console.log(h)
  // ellipse(380, 540, 100-(h/10), h/20);

  let h = map(vol, 0, 1, 0, 18);
  console.log(Math.round(h));
  let index = Math.round(h);
  // console.log(boca[h]);
  image(boca[index], 335, 480, 100, 100);
}